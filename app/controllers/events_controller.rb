class EventsController < ApplicationController
  before_action :set_even, :only => [:show, :edit, :update, :destoy]

  def set_evet
    @event = Event.find(event_id)
  end

  def index
    @events = Event.all
  end

  def new
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:notice] = 'event was successfully created'  
      redirect_to :action => :index
    else
      falsh[:alert] = 'Event create fail'
      render :action => :new
    end
  end
    
  def destoy
    @event.distory
    redirect_to :action => :index
  end

  def update
    if @event.update(event_params)
      flash[:notice] = 'event was successully updated'
      redirect_to :action => :show, :id => @event.id
    else
      flash[:alert] = 'Event update fail'
      render :action => :edit
    end
  end

  def show
    @event_title = @event.name
  end

  private
    def event_params
      params.require(:event).premit(:name, :description)
    end

    def event_id
      params[:id]
    end
end
